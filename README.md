# Introduction

This project creates a docker image with texlive installed.

The image can be used as a base image for other images or to create documents renered by (la)tex.

This repository is mirrored to https://gitlab.com/sw4j-net/texlive
